export default function secondsToTimeString(totalSeconds) {
  let minutes = window.parseInt(totalSeconds / 60);
  let seconds = window.parseInt(totalSeconds % 60);
  return minutes + ':' + (seconds < 10 ? ('0' + seconds) : seconds);
}
