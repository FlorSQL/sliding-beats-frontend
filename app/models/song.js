import DS from 'ember-data';

const { attr, belongsTo } = DS;

export default DS.Model.extend({
  title: attr('string'),
  duration: attr('number'),
  bpm: attr('number'),

  album: belongsTo('album', {
    async: false
  }),

  howl: null,
  isPlaying: false
});
