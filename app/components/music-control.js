import Ember from 'ember';

const { computed } = Ember;

export default Ember.Component.extend({
  classNames: ['music-control'],
  playingSong: Ember.inject.service(),
  isLeftPlayerAvailable: computed.none('leftSong'),
  isRightPlayerAvailable: computed.none('rightSong'),

  didUpdateAttrs() {
    this._super(...arguments);
    if (!Ember.isNone(this.get('pendingSong'))) {
      if (this.get('isLeftPlayerAvailable')) {
        this.set('leftSong', this.get('pendingSong'));
      } else if (this.get('isRightPlayerAvailable')) {
        this.set('rightSong', this.get('pendingSong'));
      }
    }
  },

  actions: {
    changeSongPlayingState(song, isPlaying) {
      song.set('isPlaying', isPlaying);
    }
  }
});
