import Ember from 'ember';
import config from 'sliding-beats-frontend/config/environment';
import secondsToTimeString from '../utils/seconds-to-time-string';

const { computed } = Ember;

export default Ember.Component.extend({
  classNames: ['player'],
  classNameBindings: [
    'isLeft:player-left:player-right',
    'isActive::disabled'
  ],
  playingSongs: Ember.inject.service(),

  isActive: computed('isAvailable', 'songIsLoaded', function() {
    return !this.get('isAvailable') && this.get('songIsLoaded');
  }).readOnly(),

  elapsedTimeFormatted: computed('elapsedTime', function() {
    if (Ember.isEmpty(this.get('song'))) return '0:00';
    return this.calculateTimeFormatted(this.get('elapsedTime'));
  }).readOnly(),

  durationFormatted: computed('song.duration', function() {
    if (Ember.isEmpty(this.get('song'))) return '0:00';
    return this.calculateTimeFormatted(this.get('song.duration'));
  }).readOnly(),

  songChanged: function() {
    if (!Ember.isNone(this.get('song'))) {
      let options = {};
      if (!this.get('playingSongs.isAnySongPlaying')) options.play = true;
      this.send('loadSong', this.get('song'), options);
    }
  }.observes('song'),

  calculateTimeFormatted(seconds) {
    if (Ember.isNone(this.get('song'))) return '0:00';
    return secondsToTimeString(seconds);
  },

  updatePlayer() {
    this.set('elapsedTime', this.get('song.howl').seek() * this.get('song.howl').rate());
    // console.log(this.get('song.howl').seek());
  },

  setUpdateInterval() {
    (this.get('updatePlayer').bind(this))();
    this.set('updateInterval', window.setInterval(
      this.get('updatePlayer').bind(this),
      1000 / this.get('song.howl').rate()
    ));
  },

  clearUpdateInterval() {
    window.clearInterval(this.get('updateInterval'));
    this.set('updateInterval', null);
    (this.get('updatePlayer').bind(this))();
  },

  addSongToPlayingSongs() {
    this.set(
      this.get('isLeft') ? 'playingSongs.leftSong' : 'playingSongs.rightSong',
      this.get('song')
    );
  },

  removeSongFromPlayingSongs() {
    this.set(
      this.get('isLeft') ? 'playingSongs.leftSong' : 'playingSongs.rightSong',
      null
    );
  },

  actions: {
    loadSong(song, options) {
      let _this = this;
      song.set('howl', new Howl({
        src: [`${config.apiHost}/songs/${song.get('id')}/download.mp3`],
        onload() {
          window.song = song;
          _this.set('songIsLoaded', true);

          if (options.play === true) _this.send('play');
        },
        onrate() {
          _this.clearUpdateInterval();
          _this.setUpdateInterval();
        },
        onend() {
          _this.attrs['on-change-playing-state'](false);
          _this.removeSongFromPlayingSongs();
          _this.clearUpdateInterval();
        }
      }));
    },

    play() {
      this.get('song.howl').play();
      this.attrs['on-change-playing-state'](true);
      this.addSongToPlayingSongs();
      this.setUpdateInterval();
    },

    pause() {
      this.get('song.howl').pause();
      this.attrs['on-change-playing-state'](false);
      this.removeSongFromPlayingSongs();
      this.clearUpdateInterval();
    },

    changePlaybackPosition(position) {
      let scaledPosition = position / this.get('song.howl').rate();
      this.get('song.howl').seek(this.get('song.duration') * scaledPosition);
      this.updatePlayer();
    }
  }
});
