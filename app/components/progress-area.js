import Ember from 'ember';
import secondsToTimeString from '../utils/seconds-to-time-string';

export default Ember.Component.extend({
  classNames: ['pb-progress-area'],

  pxToPercentage(pixels) {
    return pixels / window.parseInt(this.$().css('width')) * 100;
  },

  didRender() {
    this._super(...arguments);

    let elapsedPercentage;
    if (!Ember.isNone(this.get('elapsedTime')) && !Ember.isNone(this.get('duration'))) {
      elapsedPercentage = (this.get('elapsedTime') / this.get('duration')) * 100;
    } else {
      elapsedPercentage = 0;
    }

    this.set('$elapsed-bar', this.$('.elapsed-bar'));
    this.set('$elapsed-handle', this.$('.elapsed-handle'));
    this.set('$fast-tooltip', this.$('.progress-fast-tooltip'));

    this.get('$elapsed-bar').css('width', elapsedPercentage + '%');
    this.get('$elapsed-handle').css('left', `calc(${elapsedPercentage}% - 5px)`);
  },

  click(event) {
    if (!this.get('isDisabled')) {
      let offsetLeft = this.getOffsetLeft(event);
      let clickPlaybackPosition = this.pxToPercentage(offsetLeft) / 100;
      this.attrs['on-change-playback-position'](clickPlaybackPosition);
    }
  },

  mouseMove(event){
    if (Ember.isNone(this.get('throttledUpdateTooltip'))) {
      this.set('throttledUpdateTooltip', _.throttle(
        this.get('updateTooltip').bind(this), 10
      ));
    }

    this.get('throttledUpdateTooltip')(event);
  },

  mouseLeave() {
    this.get('$fast-tooltip').removeClass('visible');
  },

  getOffsetLeft(event) {
    if (Ember.$(event.target).hasClass('elapsed-handle')) {
      return event.target.offsetLeft + 5;
    } else if (Ember.$(event.target).hasClass('progress-fast-tooltip')) {
      return event.target.offsetLeft
        + parseFloat(this.get('$fast-tooltip').css('width')) / 2;
    }
    return event.offsetX;
  },

  updateTooltip(event) {
    if (!this.get('isDisabled')) {
      if (Ember.$(event.target).hasClass('progress-fast-tooltip')) return;

      let offsetLeft = this.getOffsetLeft(event);
      let percentageWhereMouseIs = this.pxToPercentage(offsetLeft);
      this.set('hoveredPosition', secondsToTimeString(
        this.get('duration') * percentageWhereMouseIs / 100
      ));

      this.get('$fast-tooltip').addClass('visible');

      Ember.run.schedule('afterRender', () => {
        let tooltipWidth = parseFloat(this.get('$fast-tooltip').css('width'));
        let leftString, rightString;

        if(offsetLeft < tooltipWidth / 2) {
          leftString = '0';
          rightString = 'intial';
        } else if (offsetLeft + tooltipWidth / 2 > parseFloat(this.$().css('width'))) {
          leftString = 'initial';
          rightString = '0';
        } else {
          leftString = `calc(${percentageWhereMouseIs}% - ${tooltipWidth / 2}px)`;
          rightString = 'initial';
        }

        this.get('$fast-tooltip')
            .css({ left: leftString, right: rightString });
      });
    }
  }
});
