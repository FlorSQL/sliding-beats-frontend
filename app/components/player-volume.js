import Ember from 'ember';

const { $, computed } = Ember;

export default Ember.Component.extend({
  classNames: ['volume', 'u-vertical-center'],

  volume: 1,
  isVolumeUp: computed('volume', function() {
    return this.get('volume') >= 0.5
  }).readOnly(),
  isVolumeDown: computed('volume', function() {
    return this.get('volume') > 0 && this.get('volume') < 0.5
  }).readOnly(),
  isMute: computed('volume', function() {
    return this.get('volume') === 0;
  }).readOnly(),

  didUpdateAttrs() {
    this._super(...arguments);
    this.set('$sliderUsable', this.$('.volume-slider-usable'));

    let emptyImage = document.createElement('img');
    this.get('$sliderUsable').on({
      'drag': _.throttle(this.changeVolume, 10).bind(this),
      'dragstart': event => {
        event.dataTransfer.setDragImage(emptyImage, 0, 0);
      },
      // Keep slider opened in case you got too high or too low on the page
      'dragend': event => {
        this.$('.volume-slider').addClass('forced-active');
        if (!Ember.isEmpty(this.get('removeSliderActiveClassSchedule'))) {
          Ember.run.cancel(this.get('removeSliderActiveClassSchedule'));
        }
        this.set('removeSliderActiveClassSchedule', Ember.run.later(() => {
          this.$('.volume-slider').removeClass('forced-active');
        }, 500));
      }
    });
  },

  willDestroy() {
    this._super(...arguments);
    this.get('$sliderUsable').off('drag dragstart dragend');
  },

  click(event) {
    this.changeVolume(event);
  },

  dragOver(event) {
    return false;
  },

  setVolume(value) {
    this.get('song.howl').volume(value);
    this.$('.volume-bar-current').height(value * 100 + '%');
    this.set('volume', this.get('song.howl').volume());
  },

  changeVolume(event) {
    let $sliderUsable = this.get('$sliderUsable');
    let offsetToSliderTopPx;

    if (event.type === 'drag') event = event.originalEvent;

    //browser is triggering an click outside the viewdrop on drop, no idea why
    if (event.screenY === 0) return false;

    if (event.target === $sliderUsable.get(0) || $.contains($sliderUsable.get(0), event.target)) {
      let volume;
      if (event.offsetY < 0) {
        volume = 1;
      } else if (event.offsetY > $sliderUsable.height()) {
        volume = 0;
      } else {
        offsetToSliderTopPx = event.offsetY;
        if($(event.target).hasClass('volume-bar-current')) {
          offsetToSliderTopPx += $sliderUsable.height() - $(event.target).height();
        }
        volume = 1 - offsetToSliderTopPx / $sliderUsable.height()
      }

      this.setVolume(volume);
    }
  },

  actions: {
    mute() {
      this.set('volumeBeforeMute', this.get('volume'));
      this.setVolume(0);
    },

    unmute() {
      let volBeforeMute = this.get('volumeBeforeMute');
      let newVolume = Ember.isNone(volBeforeMute) || volBeforeMute === 0
                    ? 1
                    : this.get('volumeBeforeMute');
      this.setVolume(newVolume);
      this.set('volumeBeforeMute', null);
    }
  }
});
