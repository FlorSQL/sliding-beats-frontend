import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel() {
    Howler.iOSAutoEnable = false;
  },

  actions: {
    loadSong(song) {
      this.controller.set('pendingSong', song);
    }
  }
});
