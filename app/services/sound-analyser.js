import Ember from 'ember';

export default Ember.Service.extend({
  AudioContext: window.AudioContext || window.webkitAudioContext,
  OfflineAudioContext: window.OfflineAudioContext || window.webkitOfflineAudioContext,

  getAudioContext() {
    if (Ember.isEmpty(this.get('audioContext'))) {
      this.set('audioContext', new this.AudioContext());
    }
    return this.get('audioContext');
  },

  // Function to identify peaks
  getPeaksAtThreshold(data, threshold) {
    let peaksArray = [];
    let length = data.length;
    for(let i = 0; i < length;) {
      if (data[i] > threshold) {
        peaksArray.push(i);
        // Skip forward ~ 1/4s to get past this peak.
        i += 10000;
      }
      i++;
    }
    return peaksArray;
  },

  // Function used to return a histogram of peak intervals
  countIntervalsBetweenNearbyPeaks(peaks) {
    let intervalCounts = [];
    let getCount = R.curry(function(interval, intervalCount) {
      if (intervalCount.interval === interval) {
        return intervalCount.count++;
      }
    });
    peaks.forEach((peak, index) => {
      for(let i = 0; i < 10; i++) {
        let interval = peaks[index + i] - peak;
        let foundInterval = intervalCounts.some(getCount(interval));
        if (!foundInterval) {
          intervalCounts.push({
            interval: interval,
            count: 1
          });
        }
      }
    });
    return intervalCounts;
  },

  // Function used to return a histogram of tempo candidates.
  groupNeighborsByTempo(intervalCounts, sampleRate) {
    let tempoCounts = [];
    intervalCounts.forEach(intervalCount => {
      if (intervalCount.interval !== 0) {
        // Convert an interval to tempo
        let theoreticalTempo = 60 / (intervalCount.interval / sampleRate );

        // Adjust the tempo to fit within the 90-180 BPM range
        while (theoreticalTempo < 90) {
          theoreticalTempo *= 2;
        }
        while (theoreticalTempo >= 180) {
          theoreticalTempo /= 2;
        }

        theoreticalTempo = Math.round(theoreticalTempo);
        let foundTempo = tempoCounts.some(tempoCount => {
          if (tempoCount.tempo === theoreticalTempo) {
            return tempoCount.count += intervalCount.count;
          }
        });
        if (!foundTempo) {
          tempoCounts.push({
            tempo: theoreticalTempo,
            count: intervalCount.count
          });
        }
      }
    });
    return tempoCounts;
  },

  //You have to decode audio data before estimated bpms
  decodeAudio(audioData) {
    let audioContext = this.getAudioContext();

    return new Ember.RSVP.Promise(resolve => {
      audioContext.decodeAudioData(audioData, buffer => resolve(buffer));
    });
  },

  estimateBpms(buffer) {
    let offlineContext = new this.OfflineAudioContext(
      buffer.numberOfChannels,
      buffer.length,
      buffer.sampleRate
    );

    // Create source
    let source = offlineContext.createBufferSource();
    source.buffer = buffer;

    // Create filter
    var filter = offlineContext.createBiquadFilter();
    filter.type = "lowpass";


    // Pipe the song into the filter, and the filter into the offline context
    source.connect(filter);
    filter.connect(offlineContext.destination);

    source.start();

    //PROCESSING
    let peaks;
    let initialThreshold = 0.90;
    let threshold = initialThreshold;
    let minThreshold = 0.3;
    let minPeaks = 30;

    do {
      peaks = this.getPeaksAtThreshold(buffer.getChannelData(0), threshold);
      threshold -= 0.05;
    } while (peaks.length < minPeaks && threshold >= minThreshold);

    let intervals = this.countIntervalsBetweenNearbyPeaks(peaks);

    let groups = this.groupNeighborsByTempo(intervals, buffer.sampleRate);

    return groups.sort((intA, intB) => intB.count - intA.count).splice(0,5);
  }
});
