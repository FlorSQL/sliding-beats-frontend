import Ember from 'ember';

const { computed } = Ember;

export default Ember.Service.extend({
  leftSong: null,
  rightSong: null,
  isAnySongPlaying: computed.or('leftSong', 'rightSong')
});
