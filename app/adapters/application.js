import ActiveModelAdapter from 'active-model-adapter';
import config from 'sliding-beats-frontend/config/environment';

export default ActiveModelAdapter.extend({
  host: config.apiHost
});
