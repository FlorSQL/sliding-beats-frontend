import Ember from 'ember';

export default Ember.Controller.extend({
  soundAnalyser: Ember.inject.service(),

  actions: {
    songLoaded(file) {
      let estimatedBpms;
      this.get('soundAnalyser').decodeAudio(file.data).then(buffer => {
        estimatedBpms = this.get('soundAnalyser').estimateBpms(buffer);
        console.log(estimatedBpms);
      });
    }
  }
});
