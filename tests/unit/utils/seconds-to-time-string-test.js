import secondsToTimeString from 'sliding-beats-frontend/utils/seconds-to-time-string';
import { module, test } from 'qunit';

module('Unit | Utility | seconds to time string');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = secondsToTimeString();
  assert.ok(result);
});
